<?php
require_once __DIR__. "/autoload/autoload.php";

$id =intval(getInput('id'));
$EditCategory = $db->fetchID("category",$id);

//chi tiet sp
$product = $db->fetchID("product",$id);
//_debug($product);

$cateid=intval($product['category_id']);

$sql="SELECT * FROM product WHERE category_id =$cateid ORDER BY ID DESC LIMIT 4 ";

$sanphamkemtheo = $db->fetchsql($sql);

$danhmuc = $db->categoryname($cateid);

?>

<?php  require_once __DIR__. "/layouts/header.php";?>

<!-- Start Product Details -->
<div class="container">
	<div class="product-quick-view">
		<div class="row align-items-center">
			<div class="col-lg-6">
				<!-- <img src="<?php echo uploads() ?>product/<?php echo $product['image'] ?>"> -->
				<div class="quick-view-carousel-details">
					<div class="item" style="background:
					url(<?php echo uploads() ?>product/<?php echo $product['image'] ?>);">

				</div>
				<div class="item" style="background: url(img/q4.jpg);">

				</div>

			</div>
		</div>
		<div class="col-lg-6">
			<div class="quick-view-content">
				<div class="top">
					<h3 class="head"><?php echo $product['name'] ?></h3>
					<div class="price d-flex align-items-center"><span class="lnr lnr-tag"></span>
						<?php if ($product['sale']>0): ?>
							<span class="ml-10"><?php echo formatpricesale($product['price'],$product['sale'])?></span>
						  &ensp;<strike><?php echo formatPrice($product['price']) ?></strike>
						 <?php else: ?>
						 	<span class="ml-10"><?php echo formatpricesale($product['price'],$product['sale'])?></span>
						<?php endif ?>
					</div>
					<div class="category">Danh mục: <span><?php echo $danhmuc["name"]; ?></span></div>
					<div class="available">Số lượng còn lại: <span><?php echo $product["number"]; ?></span></div>
				</div>
				<div class="middle">
					<p class="content"><?php echo $product['content'] ?></p>
				</div>
				<div >
					<div class="quantity-container d-flex align-items-center mt-15">
						Số lượng:
						<input type="text" class="quantity-amount ml-15" value="1" />
						<div class="arrow-btn d-inline-flex flex-column">
							<button class="increase arrow" type="button" title="Increase Quantity"><span class="lnr lnr-chevron-up"></span></button>
							<button class="decrease arrow" type="button" title="Decrease Quantity"><span class="lnr lnr-chevron-down"></span></button>
						</div>

					</div>
					<div class="d-flex mt-20">
						<a href="addcart.php?id=<?php echo $item['id'] ?>" class="view-btn color-2"><span>Thêm vào giỏ hàng</span></a>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="container">
	<div class="details-tab-navigation d-flex justify-content-center mt-30" style="margin-bottom: 30px;">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
			<li>
				<!-- <a class="nav-link" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-expanded="true">Description</a> -->
					<h3>Sản phẩm cùng loại</h3>		<br>	<br>

				<div class="row ">
						<?php foreach ($sanphamkemtheo as $item): ?>
							<div class="col-xl-3 col-lg-6 col-sm-12 col-sm-6 single-product">
								<div class="content">
									<div class="content-overlay"></div>
									<img class="content-image img-fluid d-block mx-auto"
									src="<?php echo uploads() ?>product/<?php echo $item['image'] ?>" alt="">
									<div class="content-details fadeIn-bottom">
										<div class="bottom d-flex align-items-center justify-content-center">
											<a href="chi-tiet-san-pham.php?id=<?php echo $item['id'] ?>">
												<span class="lnr lnr-heart"></span></a>
											<a href="#"><span class="lnr lnr-layers"></span></a>
											<a href="#"><span class="lnr lnr-cart"></span></a>
											<a href="#" data-toggle="modal" data-target="#exampleModal">
												<span class="lnr lnr-frame-expand"></span>
											</a>
										</div>
									</div>
								</div>
								<div class="price">
									<h5 ><?php echo $item['name'] ?></h5>
									<h3 ><strike><?php echo formatPrice($item['price']) ?></strike>
										<?php echo formatpricesale($item['price'],$item['sale']) ?></h3>
								</div>
							</div>
						<?php endforeach ?>
					</div>
			</li>
			<!-- <li>
				<a class="nav-link" id="specification-tab" data-toggle="tab" href="#specification" role="tab" aria-controls="specification">Specification</a>
			</li>
			<li>
				<a class="nav-link" id="comments-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments">Comments</a>
			</li>
			<li>
				<a class="nav-link active" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews">Reviews</a>
			</li> -->
		</ul>
	</div>
</div>
<!-- End Product Details -->
<?php  require_once __DIR__. "/product-detail/layouts/footer.php";?>
