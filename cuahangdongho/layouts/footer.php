 <!-- start footer Area -->
        <footer class="footer-area section-gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3  col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6>Thông tin</h6>
                            <p>
                                Chuyên bán các loại đồng hồ đeo tay cao cấp chất lượng quốc tế.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3  col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6>Newsletter</h6>
                            <p>Stay update with our latest</p>
                            <div class="" id="mc_embed_signup">
                                <form target="_blank" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="form-inline">
                                    <div class="d-flex flex-row">
                                        <input class="form-control" name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">
                                        <button class="click-btn btn btn-default"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                                        <div style="position: absolute; left: -5000px;">
                                            <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                        </div>
                                        <!-- <div class="col-lg-4 col-md-4">
                                            <button class="bb-btn btn"><span class="lnr lnr-arrow-right"></span></button>
                                            </div>  -->
                                    </div>
                                    <div class="info"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3  col-md-6 col-sm-6">
                        <div class="single-footer-widget mail-chimp">
                            <h6 class="mb-20">Instagram Feed</h6>
                            <ul class="instafeed d-flex flex-wrap">

                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6>Theo dõi tại</h6>
                            <p>Mạng xã hội</p>
                            <div class="footer-social d-flex align-items-center">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <p class="footer-text m-0">
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                        <a href="fb.com/phong254" target="_blank">PhongHa</a>
                    </p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </div>
            </div>

        </footer>
        <!-- End footer Area -->
        <script src="<?php echo base_url() ?>public/frontend/js/vendor/jquery-2.2.4.min.js"></script>
      <script src="<?php echo base_url() ?>public/frontend/js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>public/frontend/js/jquery.ajaxchimp.min.js"></script>
        <script src="<?php echo base_url() ?>public/frontend/js/jquery.nice-select.min.js"></script>
        <script src="<?php echo base_url() ?>public/frontend/js/jquery.sticky.js"></script>
        <script src="<?php echo base_url() ?>public/frontend/js/ion.rangeSlider.js"></script>
        <script src="<?php echo base_url() ?>public/frontend/js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url() ?>public/frontend/js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url() ?>public/frontend/js/main.js"></script>
    </body>
</html>
<script type="text/javascript">

     $(function(){
            $updatecart = $('.updatecart');
            $updatecart.click(function(e)){
                e.preventDefault();
                $qty =  $(this).parents("a").find.('qty').val();
                console.log($qty);
                $key= $(this).attr('data-key');
                $.ajax=({
                    url: "update-gio-hang.php",
                    type: 'GET',
                    data: {'qty':$qty,'key':$key},
                    success:function(data)
                    {
                        if(data==1)
                        {
                            alert("cập nhật thành công");
                            location.href='gio-hang.php';
                        }
                    }
                });
            }
        })
</script>
