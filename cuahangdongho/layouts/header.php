<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="favicon.ico">
        <meta name="author" content="Zonbi">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="UTF-8">
        <title>Shop đồng hồ đeo tay</title>

        <!--
            CSS
            ============================================= -->
        <link rel="stylesheet" href="<?php echo base_url() ?>public/frontend/css/linearicons.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>public/frontend/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>public/frontend/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>public/frontend/css/nice-select.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>public/frontend/css/ion.rangeSlider.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>public/frontend/css/ion.rangeSlider.skinFlat.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>public/frontend/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>public/frontend/css/main.css">
    </head>
    <body>
        <!-- Start Header Area -->
        <header class="default-header">
            <div class="menutop-wrap">
                <div class="menu-top container">
                    <div class="d-flex justify-content-between align-items-center">

                        <ul class="list">
                          <li><a href="index.php">Trang chủ</a></li>
                          <li class="dropdown">
                              <a class="dropdown-toggle" href="#catagory" id="navbardrop" data-toggle="dropdown">Danh mục sản phẩm</a>
                              <div class="dropdown-menu">
                                  <?php foreach ($category as $item) :?>
                                  <a class="dropdown-item" href="danh-muc-san-pham.php?
                                  id=<?php echo $item['id'] ?>"> <?php echo $item['name'] ?> </a>
                                  <?php endforeach; ?>
                              </div>
                          </li>
                        </ul>

                        <div align="center">
          <form action="tim-kiem-san-pham.php" method="get">
              Tìm kiếm sản phẩm: <input type="text" name="search" />
              <input type="submit" value="Tìm" />
          </form>
      </div>
                        <?php if (isset($_SESSION['name_user'])): ?>
                            <ul class="list">
                                <li style=""  >Xin chào: <?php echo $_SESSION['name_user'] ?></li>
                                <li class="dropdown">
                                <a class="dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                Tài khoản
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="category.html">Thông tin</a>
                                    <a class="dropdown-item" href="gio-hang.php">Giỏ hàng</a>
                                    <a class="dropdown-item" href="thoat.php">Đăng xuất</a>
                                </div>
                            </li>
                            </ul>
                        <?php else: ?>
                            <ul class="list">
                            <li><a href="dang-nhap.php">Đăng nhập</a></li>
                            <li><a href="dang-ky.php">Đăng ký</a></li>
                        </ul>
                        <?php endif ?>

                    </div>
                </div>
            </div>

        </header>
        <!-- End Header Area -->
        <!-- start banner Area -->
        <section class="banner-area relative" id="home">
           <!--  <div class="container-fluid">
                <div class="row fullscreen align-items-center justify-content-center">
                    <div class="col-lg-6 col-md-12 d-flex align-self-end img-right no-padding">
                        <img class="img-fluid" src="./public/uploads/banner9.webp" alt="">
                    </div>
                    <div class="banner-content col-lg-6 col-md-12">
                        <h1 class="title-top"><span>Giảm giá tới</span> 75%</h1>
                        <h1 class="text-uppercase">
                            Vào lễ Noel <br>
                             Sắp tới!
                        </h1>
                        <button class="primary-btn text-uppercase"><a href="#">Xem chi tiết</a></button>
                    </div>
                </div>
            </div> -->

        </section>
        <!-- End banner Area -->
