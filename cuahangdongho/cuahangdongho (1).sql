-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 26, 2020 at 04:43 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cuahangdongho`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `phone`, `address`, `email`, `password`) VALUES
(1, 'Nguyễn Hà Phong', '113', 'a', 'phongdz254@gmail.com', 'c4ca4238a0b923820dcc509a6f7584'),
(2, 'Phong Ha', '11445', 'q9', '123@a.s', 'z');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `home` tinyint(4) DEFAULT 0,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `images`, `banner`, `home`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Philippe Auguste', 'philippe-auguste', NULL, NULL, 1, 1, '2020-05-06 04:30:17', '2020-06-26 11:47:46'),
(2, 'Epos Swiss', 'Epos Swiss', NULL, NULL, 1, 1, '2020-05-06 04:30:32', '2020-06-08 03:34:11'),
(20, 'Atlantic Swiss', 'Atlantic Swiss', NULL, NULL, 1, 1, '2020-05-06 03:43:41', '2020-06-08 03:34:19');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` tinyint(4) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `transaction_id`, `product_id`, `qty`, `price`, `created_at`, `updated_at`) VALUES
(22, 12, 15, 1, 9500000, '2020-06-08 08:53:42', '2020-06-08 08:53:42'),
(23, 12, 1, 1, 9600000, '2020-06-08 08:53:42', '2020-06-08 08:53:42'),
(24, 13, 15, 1, 9500000, '2020-06-08 13:03:04', '2020-06-08 13:03:04'),
(25, 13, 16, 1, 6840000, '2020-06-08 13:03:04', '2020-06-08 13:03:04');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `sale` int(11) DEFAULT 0,
  `image` varchar(100) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `number` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `sale`, `image`, `category_id`, `content`, `number`) VALUES
(1, 'Đồng hồ Philippe Auguste PA5001A', 9600000, 0, 'pa5001a_dong-ho-nhap-khau.jpg', 1, 'Áo\r\nKiểu dáng:\r\n\r\nĐồng hồ Nam\r\nNăng lượng:\r\n\r\nAutomatic (Cơ tự động)\r\nĐộ chịu nước:\r\n\r\n5 ATM\r\nChất liệu mặt:\r\n\r\nSapphire\r\nĐường kính mặt:\r\n\r\n40 mm\r\nChất liệu dây:\r\n\r\nDây da chính hãng', 1000),
(15, 'Đồng hồ Philippe Auguste PA5001B', 9500000, 0, 'pa5001b_dong-ho-nhap-khau2.jpg', 1, 'Áo\r\nKiểu dáng:\r\n\r\nĐồng hồ Nam\r\nNăng lượng:\r\n\r\nAutomatic (Cơ tự động)\r\nĐộ chịu nước:\r\n\r\n5 ATM\r\nChất liệu mặt:\r\n\r\nSapphire\r\nĐường kính mặt:\r\n\r\n40 mm\r\nChất liệu dây:\r\n\r\nDây da chính hãng', 1000),
(16, 'Đồng hồ Philippe Auguste PA5001C', 7600000, 10, 'pa5001c_dong-ho-nhap-khau3.jpg', 1, 'Áo\r\nKiểu dáng:\r\n\r\nĐồng hồ Nam\r\nNăng lượng:\r\n\r\nAutomatic (Cơ tự động)\r\nĐộ chịu nước:\r\n\r\n5 ATM\r\nChất liệu mặt:\r\n\r\nSapphire\r\nĐường kính mặt:\r\n\r\n40 mm\r\nChất liệu dây:\r\n\r\nDây da chính hãng', 1000),
(17, 'Đồng hồ Philippe Auguste PA5001D', 9800000, 10, 'pa5001d_dong-ho-nhap-khau4.jpg', 1, 'Áo\r\nKiểu dáng:\r\n\r\nĐồng hồ Nam\r\nNăng lượng:\r\n\r\nAutomatic (Cơ tự động)\r\nĐộ chịu nước:\r\n\r\n5 ATM\r\nChất liệu mặt:\r\n\r\nSapphire\r\nĐường kính mặt:\r\n\r\n40 mm\r\nChất liệu dây:\r\n\r\nDây da chính hãng', 1000),
(18, 'ĐỒNG HỒ EPOS SWISS E-7000.701.20.98.25', 13000000, 10, '1367739457_DONG-HO-CHINH-HANG-2.jpg', 2, 'Áo\r\nKiểu dáng:\r\n\r\nĐồng hồ Nam\r\nNăng lượng:\r\n\r\nAutomatic (Cơ tự động)\r\nĐộ chịu nước:\r\n\r\n5 ATM\r\nChất liệu mặt:\r\n\r\nSapphire\r\nĐường kính mặt:\r\n\r\n40 mm\r\nChất liệu dây:\r\n\r\nDây da chính hãng', 1000),
(19, 'ĐỒNG HỒ EPOS SWISS E-8000.700.22.68.32', 19900000, 10, '186594578_dong-ho-chinh-hang-62.jpg', 2, 'Xuất xứ:\r\n\r\nThụy Sỹ\r\nKiểu dáng:\r\n\r\nĐồng hồ Nữ\r\nNăng lượng:\r\n\r\nQuartz/Pin\r\nĐộ chịu nước:\r\n\r\n3 ATM\r\nChất liệu mặt:\r\n\r\nKính sapphire\r\nĐường kính mặt:\r\n\r\n34 mm\r\nĐộ dầy:\r\n\r\n7mm\r\nSize dây:\r\n\r\nThép không gỉ Mạ vàng PVD\r\nChất liệu dây:\r\n\r\nThép không gỉ mạ PVD', 1000),
(20, 'ĐỒNG HỒ EPOS SWISS E-7000.701.20.95.25', 13000000, 0, '1779327189_dong-ho-chinh-hang-65.jpg', 2, 'Hãng sản xuất : Epos Swiss\r\n\r\nXuất xứ : Thụy Sỹ\r\n\r\nBảo hành: 10 năm\r\n\r\nLoại đồng hồ : Đồng hồ nam\r\n\r\nLoại kính : Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ\r\n\r\nChất liệu dây : Dây da\r\n\r\nĐường kính vỏ : 41,5 mm\r\n\r\nĐộ dày vỏ : 7 mm\r\n\r\nMức độ chịu nước : 5 ATM\r\n\r\nNăng lượng sử dụng : Quartz/ Pin\r\n\r\nTư vấn và đặt hàng: 098.668.1189\r\n\r\nGiao hàng tận nơi, thanh toán trực tiếp khi nhận hàng\r\n\r\nMiễn phí vận chuyển', 9999),
(21, 'ĐỒNG HỒ EPOS SWISS E-7000.701.22.16.26', 14000000, 0, '9283642801702384644E-7000701221626.jpg', 2, 'Hãng sản xuất : Epos Swiss\r\n\r\nXuất xứ : Thụy Sỹ\r\n\r\nBảo hành: 10 năm\r\n\r\nLoại đồng hồ : Đồng hồ nam\r\n\r\nLoại kính : Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ mạ vàng PVD\r\n\r\nChất liệu dây : Dây da\r\n\r\nĐường kính vỏ : 41,5 mm\r\n\r\nĐộ dày vỏ : 7 mm\r\n\r\nMức độ chịu nước : 5 ATM\r\n\r\nNăng lượng sử dụng : Quartz/ Pin', 9999),
(22, 'ĐỒNG HỒ EPOS SWISS E-7000.701.22.15.25', 14000000, 0, '460677088-E-7000-701-22-15-25.jpg', 2, 'Hãng sản xuất : Epos Swiss\r\n\r\nXuất xứ : Thụy Sỹ\r\n\r\nBảo hành: 10 năm\r\n\r\nLoại đồng hồ : Đồng hồ nam\r\n\r\nLoại kính : Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ mạ vàng PVD\r\n\r\nChất liệu dây : Dây da\r\n\r\nĐường kính vỏ : 41,5 mm\r\n\r\nĐộ dày vỏ : 7 mm\r\n\r\nMức độ chịu nước : 5 ATM\r\n\r\nNăng lượng sử dụng : Quartz/ Pin', 9999),
(23, 'ĐỒNG HỒ EPOS SWISS E-8000.700.22.68.15', 15100000, 0, '110702752donghochinhhang82.jpg', 2, 'Hãng sản xuất : Đồng hồ Epos Swiss\r\n\r\nXuất xứ : Thụy Sỹ\r\n\r\nLoại đồng hồ : Đồng hồ nữ\r\n\r\nLoại kính : Sapphire Crystal (kính chống trầy) \r\n\r\nChất liệu vỏ : Thép không gỉ mạ vàng PVD\r\n\r\nChất liệu dây: Dây da chính hãng\r\n\r\nKích thước mặt:  : 34 mm\r\n\r\nNăng lượng sử dụng : quartz (pin) \r\n\r\nTư vấn và đặt hàng: 098.668.1189\r\n\r\nBảo hành: 10 năm', 9999),
(24, 'ĐỒNG HỒ EPOS SWISS E-7000.701.20.96.26', 13000000, 0, '1125243076_dong-ho-chinh-hang-66.jpg', 2, 'Hãng sản xuất : Epos Swiss\r\n\r\nXuất xứ : Thụy Sỹ\r\n\r\nBảo hành: 10 năm\r\n\r\nLoại đồng hồ : Đồng hồ nam\r\n\r\nLoại kính : Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ\r\n\r\nChất liệu dây : Dây da\r\n\r\nĐường kính vỏ : 41,5 mm\r\n\r\nĐộ dày vỏ : 7 mm\r\n\r\nMức độ chịu nước : 5 ATM\r\n\r\nNăng lượng sử dụng : Quartz/ Pin', 9999),
(25, 'ĐỒNG HỒ EPOS SWISS E-8000.700.22.88.32', 22800000, 0, '1328554038_dong-ho-chinh-hang-67.jpg', 2, 'Hãng sản xuất : Epos Swiss\r\n\r\nXuất xứ : Thụy Sỹ\r\n\r\nBảo hành : 10 năm\r\n\r\nLoại đồng hồ : Đồng hồ nữ\r\n\r\nLoại kính : Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ mạ vàng PVD\r\n\r\nChất liệu dây : Thép không gỉ mạ vàng PVD\r\n\r\nChi tiết : Đính kim cương tự nhiên\r\n\r\nĐường kính vỏ : 34 mm\r\n\r\nĐộ dày vỏ : 7 mm\r\n\r\nMức độ chịu nước : 3 ATM\r\n\r\nNăng lượng sử dụng : Pin', 9999),
(26, 'Đồng hồ Atlantic Swiss AT-29037.45.21L', 8580000, 0, '1867397475_dong-ho-chinh-hang-1.jpg', 20, 'Hãng sản xuất: Atlantic\r\n\r\nKiểu dáng: Đồng hồ nữ\r\n\r\nChất liệu dây: Dây da\r\n\r\nChất liệu mặt:  Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ  mạ vàng PVD\r\n\r\nĐường kính vỏ : 33 mm\r\n\r\nChống nước: 3 ATM\r\n\r\nBảo hành: 10 năm\r\n\r\nNăng lượng sử dụng: Quartz\r\n\r\nThương hiệu: Thụy Sỹ', 9999),
(27, 'ĐỒNG HỒ ATLANTIC AT-61352.45.21', 9980000, 0, '2007657172_dong-ho-nu-thoi-trang-3.jpg', 20, 'Hãng sản xuất: Atlantic\r\n\r\nKiểu dáng: Đồng hồ nam\r\n\r\nChất liệu dây: dây da\r\n\r\nChất liệu mặt:  Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ mạ vàng PVD\r\n\r\nĐường kín vỏ : 42mm\r\n\r\nChống nước: 5 ATM\r\n\r\nNăng lượng sử dụng: Quartz\r\n\r\nBảo hành: 10 năm\r\n\r\nThương hiệu: Thụy Sỹ', 9999),
(28, 'ĐỒNG HỒ ATLANTIC SWISS AT-60347.45.21', 9180000, 0, '824256149_Untitled-17.jpg', 20, 'Hãng sản xuất: Atlantic\r\n\r\nKiểu dáng: Đồng hồ nam\r\n\r\nChất liệu dây: Thép không gỉ mạ vàng PVD\r\n\r\nChất liệu mặt:  Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ mạ vàng PVD\r\n\r\nKích thước vỏ: 40 mm\r\n\r\nĐộ dày vỏ: 5 mm\r\n\r\nChịu nước: 5 ATM\r\n\r\nNăng lượng sử dụng: pin\r\n\r\nBảo hành: 10 năm\r\n\r\nThương hiệu: Thụy Sỹ\r\n\r\nMáy: Thụy Sỹ', 9999),
(29, 'ĐỒNG HỒ ATLANTIC SWISS AT-29035.41.21', 7620000, 0, '1900541705_dong-ho-chinh-hang-2.jpg', 20, 'Hãng sản xuất: Atlantic\r\n\r\nLoại đồng hồ: Đồng hồ nữ\r\n\r\nChất liệu dây: Thép không gỉ \r\n\r\nChất liệu mặt: sapphire glass ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ \r\n\r\nĐường kính vỏ :27 mm\r\n\r\nĐộ dày vỏ: 5 mm\r\n\r\nChống nước: 3 bar\r\n\r\nNăng lượng sử dụng: Quartz\r\n\r\nBảo hành: 10 năm\r\n\r\nThương hiệu: Thụy Sỹ\r\n\r\nMáy: Thụy Sỹ', 9999),
(30, 'ĐỒNG HỒ ATLANTIC SWISS AT-60347.43.31', 8980000, 0, '2013794039_Untitled-75.jpg', 20, 'Hãng sản xuất: Atlantic\r\n\r\nChất liệu dây: Thép không gỉ (mạ Demi PVD )\r\n\r\nChất liệu mặt:  Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ (mạ Demi PVD )\r\n\r\nNăng lượng sử dụng: Quartz\r\n\r\nĐộ chịu nước : 5 ATM\r\n\r\nĐường kính: 40 mm\r\n\r\nBảo hành: 10 năm\r\n\r\nThương hiệu: Thụy Sỹ', 9999),
(31, 'ĐỒNG HỒ ATLANTIC SWISS AT-62341.45.61', 7990000, 0, '854920604_Untitled-44.jpg', 20, 'Hãng sản xuất: Atlantic\r\n\r\nChất liệu dây: Dây da\r\n\r\nChất liệu mặt:  Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ mạ vàng PVD\r\n\r\nNăng lượng sử dụng: Quartz\r\n\r\nĐộ chịu nước : 3 ATM\r\n\r\nĐường kính: 40 mm\r\n\r\nĐộ dày: 6 mm\r\n\r\nBảo hành: 10 năm\r\n\r\nThương hiệu: Thụy Sỹ\r\n\r\nMáy: Thụy Sỹ', 9999),
(32, 'ĐỒNG HỒ ATLANTIC SWISS AT-60342.45.11', 8390000, 0, '347591995_Untitled-29.jpg', 20, 'Đồng hồ Atlantic đến từ đất nước Thụy Sỹ sẽ luôn cho bạn thấy được sự sang trọng và luôn mạnh mẽ.\r\n\r\n \r\n\r\n\r\n\r\n \r\n\r\nKính sapphire chống trầy xước với mọi va chạm, độ trong suốt cao và sáng hơn.\r\n\r\n \r\n\r\n\r\n\r\n \r\n\r\nChống nước 5 ATM thoải mái hoạt động mà không lo vào nước, mạ vàng PVD giữ cho màu luôn đẹp và bền lâu.\r\n\r\n \r\n\r\n\r\n\r\n \r\n\r\nLịch thiệp với thiết kế dây da, trẻ trung, mạnh mẽ nhưng quan trọng vẫn là sự thoải mái cho người đeo.\r\n\r\n \r\n\r\n\r\n\r\n \r\n\r\n \r\n\r\n \r\n\r\nHãng sản xuất: Atlantic\r\n\r\nChất liệu dây: Dây da\r\n\r\nChất liệu mặt:  Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ mạ vang PVD\r\n\r\nNăng lượng sử dụng: Quartz\r\n\r\nĐộ chịu nước : 5 ATM\r\n\r\nĐường kính: 40 mm\r\n\r\nBảo hành: 10 năm', 9999),
(33, 'ĐỒNG HỒ ATLANTIC SWISS AT-62346.45.21', 9180000, 0, '1705990443_dong-ho-atlantic-62346.45.21.jpg', 20, 'Hãng sản xuất: Atlantic\r\n\r\nKiểu dáng: Đồng hồ nam\r\n\r\nChất liệu dây: Thép không gỉ mạ vàng PVD\r\n\r\nChất liệu mặt:  Sapphire ( Kính chống trầy )\r\n\r\nChất liệu vỏ : Thép không gỉ mạ vàng PVD\r\n\r\nĐường kính vỏ : 40mm\r\n\r\nChống nước: 3 bar\r\n\r\nBảo hành: 10 năm\r\n\r\nNăng lượng sử dụng: Quartz\r\n\r\nThương hiệu: Thụy Sỹ', 9999);

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL,
  `status` tinyint(11) DEFAULT 0,
  `note` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `amount`, `users_id`, `status`, `note`, `created_at`, `updated_at`) VALUES
(9, 50000, 8, 1, '', NULL, '2020-06-08 08:46:36'),
(12, 19150000, 21, 0, '', NULL, NULL),
(13, 16390000, 21, 0, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `address`, `password`) VALUES
(5, 'Phong Ha', '1@gmail.com', 327453737, 'q9', '202cb962ac59075b964b07152d234b'),
(6, 'test', 'test@gmail.com', 1234, 'gv', 'e10adc3949ba59abbe56e057f20f88'),
(8, 'anh', 'anh@gmail.com', 1212, 'q9dfdf', '123'),
(21, 'zonbita', 'zonbita@gmail.com', 1111, 'ada', 'z1z1z1z'),
(22, 'abc', 'abc@gmail.com', 1, '1', 'z');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
