b<?php
require_once __DIR__. "/autoload/autoload.php";
//_debug($_SESSION['cart']);
$sum=0;
$amount=0;

if( !isset($_SESSION['cart']) || count($_SESSION['cart']) ==0)
{
		echo "<script>alert('Không có sản phẩm trong giỏ hàng');location.href='index.php'</script>";
}


?>

<?php  require_once __DIR__. "/layouts/header.php";?>

<!-- Start women-product Area -->
<section class="women-product-area section-gap" id="women">

	<?php if (isset($_SESSION['success'])): ?>
							<div class="alert alert-success" role="alert">
							  <?php echo $_SESSION['success']; unset($_SESSION['success']) ?>
							</div>
						<?php endif ?>
	<div class="container">
		<div class="cart-title">
			<div class="row">
				<div class="col-md-1">
					<h6 class="ml-15">STT</h6>
				</div>
				<div class="col-md-2">
					<h6 class="ml-15">Tên sp</h6>
				</div>
				<div class="col-md-2">
					<h6 class="ml-15">Hình ảnh</h6>
				</div>
				<div class="col-md-2">
					<h6>Giá</h6>
				</div>
				<div class="col-md-2">
					<h6>Số lượng</h6>
				</div>
				<div class="col-md-2">
					<h6>Total</h6>
				</div>
				<div class="col-md-1">
					<h6>Thao tác</h6>
				</div>
			</div>
		</div>

		<?php $sum = 0; ?>
		<?php $stt=1; foreach ($_SESSION['cart'] as $key => $value): ?>
		<tr>
			<div class="cart-single-item">
			<div class="row align-items-center">
				<div class="col-md-1 col-12">
					<div><?php echo $stt ?></div>
				</div>
				<div class="col-md-2 col-12">
					<div class="product-item d-flex align-items-center">
						<h6><?php echo $value['name'] ?></h6>
					</div>
				</div>
				<div class="col-md-2 col-12">
					<div class="img-fluid">
						<img src="<?php echo uploads() ?>product/<?php echo $value['image'] ?>"
						width="80pc" height="80px">
					</div>
				</div>
				<div class="col-md-2 col-12">
					<div class="price"><?php echo formatPrice($value['price']) ?></div>
				</div>
				<div class="col-md-2 col-12">
					<div class="quantity-container d-flex align-items-center mt-15">
						<input type="number" class="quantity-amount" onchange="change(this.value,this.key)" value="<?php echo $value['qty'];?>" id="qty" min="0"/>

						<div class="arrow-btn d-inline-flex flex-column">
							<button class="increase arrow" type="button" title="Increase Quantity"><span class="lnr lnr-chevron-up"></span></button>
							<button class="decrease arrow" type="button" title="Decrease Quantity"><span class="lnr lnr-chevron-down"></span></button>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-12">
					<div class="total"><?php echo formatPrice($value['price'] * $value['qty'])?></div>
				</div>
				<div class="col-md-1 col-12">
					<a href="remove.php?key=<?php echo $key ?>"class="btn btn-xs btn-danger fa fa-remove">Xóa</a>
				</div>
				<?php $sum = $sum +($value['price']*$value['qty']) ;
				$_SESSION['tongtien']=$sum ;?>
			</div>
		</div>
		</tr>
		<?php $stt++; endforeach ?>
		<div class="cupon-area d-flex align-items-center justify-content-between flex-wrap">
			<a class="updatecart view-btn color-2" data-key="<?php echo $key ?>"><span>Cật nhật giỏ hàng</span></a>
			<div class="cuppon-wrap d-flex align-items-center flex-wrap">
				<div class="cupon-code">
					<input type="text">
					<button class="view-btn color-2"><span>Apply</span></button>
				</div>
				<a href="#" class="view-btn color-2 have-btn"><span>Có mã khuyến mãi?</span></a>
			</div>
		</div>
		<div class="subtotal-area d-flex align-items-center justify-content-end">
			<div class="title text-uppercase">Số tiền</div>
			<div class="subtotal"><?php echo formatPrice($_SESSION['tongtien']) ?></div>
		</div>
		<div class="subtotal-area d-flex align-items-center justify-content-end">
			<div class="title text-uppercase">Phí vận chuyển</div>
			<div class="subtotal">50.000d</div>
		</div>
		<div class="subtotal-area d-flex align-items-center justify-content-end">
			<div class="title text-uppercase">Tổng tiền</div>
			<div class="subtotal"><?php $_SESSION['total'] = $_SESSION['tongtien']+50000;
			echo formatPrice($_SESSION['total'])?></div>
		</div>
		<div class="subtotal-area d-flex align-items-center justify-content-end">
			<a href="index.php" class="view-btn color-2"><span>Tiếp tục mua hàng</span></a>
			<a href="thanh-toan.php" class="view-btn color-2"><span>Thanh toán</span></a>
		</div>

	</div>
</section>
<!-- End women-product Area -->
<?php  require_once __DIR__. "/layouts/footer.php";?>
<script type="text/javascript">
		function change(value,key){
			$_SESSION['cart'] as $key => $value
		}
</script>
