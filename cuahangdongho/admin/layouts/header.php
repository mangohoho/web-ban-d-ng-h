<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Trang Admin</title>
        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>public/admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <!-- Page level plugin CSS-->
        <link href="<?php echo base_url() ?>public/admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>public/admin/css/sb-admin.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
            <a class="navbar-brand mr-1" href="index.html">Xin chào Admin</a>
            <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
            </button>

            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <div class="input-group">

                </div>
            </form>

            <ul class="navbar-nav ml-auto ml-md-0">

                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user-circle fa-fw"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                      <!--  <a class="dropdown-item" href="#">Settings</a>
                        <a class="dropdown-item" href="#">Activity Log</a> -->
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Thoát</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="wrapper">
            <!-- Sidebar -->
            <ul class="sidebar navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url() ?>/admin">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Bảng điều khiển</span>
                    </a>
                </li>
                <li class="<?php echo isset($open) && $open =='category' ? 'active' : '' ?>">
                    <a class="nav-link" href="<?php echo modules("category") ?>">
                    <i class="fas fa-fw fas fa-list "></i>
                    <span>Danh mục sản phẩm</span>
                    </a>
                </li>
                 <li class="<?php echo isset($open) && $open=='product' ? 'active' : '' ?>">
                    <a class="nav-link" href="<?php echo modules("product") ?>">
                    <i class="fas fa-fw fas fa-database"></i>
                    <span>Sản phẩm</span>
                    </a>
                </li>
                <li class="<?php echo isset($open) && $open=='admin' ? 'active' : '' ?>">
                    <a class="nav-link" href="<?php echo modules("admin") ?>">
                    <i class="fas fa-fw fas fa-user-tie"></i>
                    <span>Admin</span>
                    </a>
                </li>
                <li class="<?php echo isset($open) && $open=='user' ? 'active' : '' ?>">
                    <a class="nav-link" href="<?php echo modules("user") ?>">
                    <i class="fas fa-fw fas fa-user-tie"></i>
                    <span>Tài khoản khách hàng</span>
                    </a>
                </li>
                <li class="<?php echo isset($open) && $open=='transaction' ? 'active' : '' ?>">
                    <a class="nav-link" href="<?php echo modules("transaction") ?>">
                    <i class="fas fa-fw fas fa-user-tie"></i>
                    <span>Đơn hàng</span>
                    </a>
                </li>

            </ul>
            <div id="content-wrapper">
                <div class="container-fluid">


                  <?php

                  if( !isset($_SESSION['name_id']))
                  {
                  	echo "<script>alert('Bạn phai đăng nhập');location.href='/../../cuahangdongho/admin/login.php'</script>";

                  }

                  ?>
