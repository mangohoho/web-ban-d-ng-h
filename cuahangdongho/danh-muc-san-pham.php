<?php
require_once __DIR__. "/autoload/autoload.php";

	$id =intval(getInput('id'));
	$EditCategory = $db->fetchID("category",$id);

	if(isset($_GET['p']))
	{
		$p=$_GET['p'];
	}
	else
	{
		$p=1;
	}

	$sql = "SELECT * FROM product WHERE category_id = $id";
	$total=count($db->fetchsql($sql));

	$product= $db->fetchJones("product",$sql,$total,$p,4,true);
	$sotrang=$product['page'];
	unset($product['page']);

	$path=$_SERVER['SCRIPT_NAME'];

?>

<?php  require_once __DIR__. "/layouts/header.php";?>

<!-- Start women-product Area -->

<div class="countdown-content pb-40">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-8 col-md-7">
				<!-- Start Filter Bar -->
				<!-- <div class="filter-bar d-flex flex-wrap align-items-center">
					<a href="#" class="grid-btn active"><i class="fa fa-th" aria-hidden="true"></i></a>
					<a href="#" class="list-btn"><i class="fa fa-th-list" aria-hidden="true"></i></a>
					<div class="sorting">
						<select>
							<option value="1">Default sorting</option>
							<option value="1">Default sorting</option>
							<option value="1">Default sorting</option>
						</select>
					</div>
					<div class="sorting mr-auto">
						<select>
							<option value="1">Show 12</option>
							<option value="1">Show 12</option>
							<option value="1">Show 12</option>
						</select>
					</div>
					<div class="pagination">
						<a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
						</a>
						<a href="#" class="active">1</a>
						<a href="#">2</a>
						<a href="#">3</a>
						<a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
						<a href="#">6</a>
						<a href="#" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
					</div>
				</div> -->
				<!-- End Filter Bar -->
				<!-- Start Best Seller -->
				<section class="lattest-product-area pb-40 category-list">
					<br><br>
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-40">
							<div class="title text-center">
							<h1 class="mb-10"><?php echo $EditCategory['name'] ?></h1>

							<p></p>
							</div>
						</div>
					</div>

					<div class="row ">
						<?php foreach ($product as $item): ?>
							<div class="col-xl-3 col-lg-6 col-md-12 col-sm-6 single-product">
								<div class="content">
									<div class="content-overlay"></div>
									<img class="content-image img-fluid d-block mx-auto"
									src="<?php echo uploads() ?>product/<?php echo $item['image'] ?>" alt="">
									<div class="content-details fadeIn-bottom">
										<div class="bottom d-flex align-items-center justify-content-center">
											<!-- <a href="chi-tiet-san-pham.php?id=<?php echo $item['id'] ?>">
												<span class="lnr lnr-heart"></span></a> -->
											<!-- <a href="#"><span class="lnr lnr-layers"></span></a> -->
											<a href="addcart.php?id=<?php echo $item['id'] ?>"><span class="lnr lnr-cart"></span></a>
											<a href="chi-tiet-san-pham.php?id=<?php echo $item['id'] ?>">
												<span class="lnr lnr-frame-expand"></span>
											</a>
										</div>
									</div>
								</div>
								<div class="price">
									<h5 ><?php echo $item['name'] ?></h5>
									<h3 ><?php if ($item['sale']>0): ?>
							<span class="ml-10"><?php echo formatpricesale($item['price'],$item['sale'])?></span>
						  &ensp;<strike><?php echo formatPrice($item['price']) ?></strike>
						 <?php else: ?>
						 	<span class="ml-10"><?php echo formatpricesale($item['price'],$item['sale'])?></span>
						<?php endif ?></h3>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				</section>
				<!-- End Best Seller -->
				<!-- Start Filter Bar -->
				<div class="filter-bar d-flex flex-wrap align-items-center">
					<!-- <div class="sorting mr-auto">
						<select>
							<option value="1">Show 12</option>
							<option value="1">Show 12</option>
							<option value="1">Show 12</option>
						</select>
					</div> -->
					<div class="pagination">
						<!-- <a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a> -->
						<?php for ($i=1; $i <= $sotrang ; $i++) :?>
							<a class="<?php echo isset($_GET['p']) && $_GET['p']==$i ? 'active' : '' ?>"
								href="<?php echo $path ?>?id=<?php echo $id ?>&&p=<?php echo $i ?>">
								<?php echo $i ?></a>
						<?php endfor; ?>
						<!-- <a href="#" class="active">1</a>
						<a href="#">2</a>
						<a href="#">3</a>
						<a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
						<a href="#">6</a>
						<a href="#" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> -->
					</div>
				</div>
				<!-- End Filter Bar -->
				</div>
				<!-- <div class="col-xl-3 col-lg-4 col-md-5">
					<div class="sidebar-categories">
						<div class="head">Browse Categories</div>
						<ul class="main-categories">
							<li class="main-nav-list"><a data-toggle="collapse" href="#beverages" aria-expanded="false" aria-controls="beverages"><span class="lnr lnr-arrow-right"></span>Beverages<span class="number">(24)</span></a>
								<ul class="collapse" id="beverages" data-toggle="collapse" aria-expanded="false" aria-controls="beverages">
									<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
									<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
									<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
								</ul>
							</li>
							<li class="main-nav-list"><a data-toggle="collapse" href="#homeClean" aria-expanded="false" aria-controls="homeClean"><span class="lnr lnr-arrow-right"></span>Home and Cleaning<span class="number">(53)</span></a>
								<ul class="collapse" id="homeClean" data-toggle="collapse" aria-expanded="false" aria-controls="homeClean">
									<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
									<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
									<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
								</ul>
							</li>
							<li class="main-nav-list"><a href="#">Pest Control<span class="number">(24)</span></a></li>
							<li class="main-nav-list"><a data-toggle="collapse" href="#officeProduct" aria-expanded="false" aria-controls="officeProduct"><span class="lnr lnr-arrow-right"></span>Office Products<span class="number">(77)</span></a>
								<ul class="collapse" id="officeProduct" data-toggle="collapse" aria-expanded="false" aria-controls="officeProduct">
									<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
									<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
									<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
								</ul>
							</li>
							<li class="main-nav-list"><a data-toggle="collapse" href="#beauttyProduct" aria-expanded="false" aria-controls="beauttyProduct"><span class="lnr lnr-arrow-right"></span>Beauty Products<span class="number">(65)</span></a>
								<ul class="collapse" id="beauttyProduct" data-toggle="collapse" aria-expanded="false" aria-controls="beauttyProduct">
									<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
									<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
									<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
								</ul>
							</li>
							<li class="main-nav-list"><a href="#">Pet Care<span class="number">(29)</span></a></li>
							<li class="main-nav-list"><a data-toggle="collapse" href="#homeAppliance" aria-expanded="false" aria-controls="homeAppliance"><span class="lnr lnr-arrow-right"></span>Home Appliances<span class="number">(15)</span></a>
								<ul class="collapse" id="homeAppliance" data-toggle="collapse" aria-expanded="false" aria-controls="homeAppliance">
									<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
									<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
									<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
								</ul>
							</li>
							<li class="main-nav-list"><a class="border-bottom-0" data-toggle="collapse" href="#babyCare" aria-expanded="false" aria-controls="babyCare"><span class="lnr lnr-arrow-right"></span>Baby Care<span class="number">(48)</span></a>
								<ul class="collapse" id="babyCare" data-toggle="collapse" aria-expanded="false" aria-controls="babyCare">
									<li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
									<li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
									<li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
									<li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
									<li class="main-nav-list child"><a href="#" class="border-bottom-0">Meat<span class="number">(11)</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="sidebar-filter mt-50">
						<div class="top-filter-head">Product Filters</div>
						<div class="common-filter">
							<div class="head">Active Filters</div>
							<ul>
								<li class="filter-list"><i class="fa fa-window-close" aria-hidden="true"></i>Gionee (29)</li>
								<li class="filter-list"><i class="fa fa-window-close" aria-hidden="true"></i>Black with red (09)</li>
							</ul>
						</div>
						<div class="common-filter">
							<div class="head">Brands</div>
							<form action="#">
								<ul>
									<li class="filter-list"><input class="pixel-radio" type="radio" id="apple" name="brand"><label for="apple">Apple<span>(29)</span></label></li>
									<li class="filter-list"><input class="pixel-radio" type="radio" id="asus" name="brand"><label for="asus">Asus<span>(29)</span></label></li>
									<li class="filter-list"><input class="pixel-radio" type="radio" id="gionee" name="brand"><label for="gionee">Gionee<span>(19)</span></label></li>
									<li class="filter-list"><input class="pixel-radio" type="radio" id="micromax" name="brand"><label for="micromax">Micromax<span>(19)</span></label></li>
									<li class="filter-list"><input class="pixel-radio" type="radio" id="samsung" name="brand"><label for="samsung">Samsung<span>(19)</span></label></li>
								</ul>
							</form>
						</div>
						<div class="common-filter">
							<div class="head">Price</div>
							<div class="price-range-area">
								<div id="price-range"></div>
								<div class="value-wrapper d-flex">
									<div class="price">Price:</div>
									<span>$</span><div id="lower-value"></div> <div class="to">to</div>
									<span>$</span><div id="upper-value"></div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<!-- End women-product Area -->
<?php  require_once __DIR__. "/layouts/footer.php";?>
