<?php
require_once __DIR__. "/autoload/autoload.php";

if(isset($_SESSION['name_user']))
{
		echo "<script>alert('Bạn đã đăng nhập tài khoản');location.href='index.php'</script>";

}

$data=
    [
        "name"=>    postInput('name'),
        "email"=>    postInput('email'),
        "phone"=>    postInput('phone'),
        "password"=>  (postInput('password')),
        "address"=>  postInput('address'),
    ];
$error=[];
if($_SERVER["REQUEST_METHOD"]=="POST")
{
	// if($data['name'] == '')
 //    {
 //        $error['name']="Bạn phải nhập họ tên";
 //    }
 //    if($data['email'] == '')
 //    {
 //        $error['email']="Mời bạn nhập email";
 //    }
 //    else
 //    {
 //    	$is_check=$db->fetchOne("users","email= '".$data['email']."'  ");
	// 	if($is_check != NULL)
	// 	{
	// 		$error['email']="Email đã tồn tại";
	// 	}
 //    }
 //    if($data['phone'] == '')
 //    {
 //        $error['phone']="Mời bạn nhập sdt";
 //    }
 //    if($data['address'] == '')
 //    {
 //        $error['address']="Mời bạn nhập địa chỉ";
 //    }
 //   	if($data['password'] == '')
 //    {
 //        $error['password']="Mời bạn nhập mật khẩu";
 //    }

	// if($data['email'] != '')
	// {
	// 	$is_check=$db->fetchOne("users","email= '".$data['email']."'  ");
	// 	if($is_check != NULL)
	// 	{
	// 		$error['email']="Email đã tồn tại";
	// 	}
	// }

	// if($data['password'] != '')
	// {
	// 	$data['password']=md5(postInput("password"));
	// }

	$idinert=$db->insert("users", $data);

	if($idinert > 0)
	{
		$_SESSION['success']="Đăng kí thành công. Mời bạn đăng nhập !";
		header("location: dang-nhap.php");
	}
	else
	{
		echo "Đk thất bại";
	}
}


?>

<?php  require_once __DIR__. "/layouts/header.php";?>

<!-- Start women-product Area -->
<section class="women-product-area section-gap" id="women">
	<!-- Start My Account -->
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="register-form">
					<h3 class="billing-title text-center">Đăng kí</h3>
					<p class="text-center mt-40 mb-30">Tạo tài khoản của riêng bạn </p>
					<form action="" method="POST">
						<input type="text" name="name" placeholder="Full name*" onfocus="this.placeholder=''"
						onblur="this.placeholder = 'Full name*'" require class="common-input mt-20">
						<?php if(isset($error['name'])): ?>
                			<p class="text-danger"> <?php echo $error['name'] ?></p>
            			<?php endif ?>
						<input type="email" name="email" placeholder="Email address*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Email address*'" require class="common-input mt-20">
						<?php if(isset($error['email'])): ?>
                			<p class="text-danger"> <?php echo $error['email'] ?></p>
            			<?php endif ?>
						<input type="number" name="phone" placeholder="Phone number*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Phone number*'" require class="common-input mt-20">
						<?php if(isset($error['phone'])): ?>
                			<p class="text-danger"> <?php echo $error['phone'] ?></p>
            			<?php endif ?>
						<input type="text" name="address" placeholder="Địa chỉ*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Địa chỉ*'" require class="common-input mt-20">
						<?php if(isset($error['address'])): ?>
                			<p class="text-danger"> <?php echo $error['address'] ?></p>
            			<?php endif ?>
						<input type="password" name="password" placeholder="Password*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Password*'"  require class="common-input mt-20">
						<?php if(isset($error['password'])): ?>
                			<p class="text-danger"> <?php echo $error['password'] ?></p>
            			<?php endif ?>
						<button type="submit" class="view-btn color-2 mt-20 w-100"><span>Đăng kí</span></button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- End My Account -->
</section>
<!-- End women-product Area -->
<?php  require_once __DIR__. "/layouts/footer.php";?>
