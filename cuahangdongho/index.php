<?php
require_once __DIR__. "/autoload/autoload.php";


$sqlHomecate="SELECT name, id FROM category WHERE home=1 ORDER BY updated_at";
$CategoryHome= $db->fetchsql($sqlHomecate);
//_debug($CategoryHome);

$data=[];
foreach ($CategoryHome as $item) {
	$cateId=intval($item['id']);

	$sql="SELECT * FROM product WHERE category_id=$cateId";
	$ProductHome=$db->fetchsql($sql);
	$data[$item['name']] = $ProductHome;
}
?>

<?php  require_once __DIR__. "/layouts/header.php";?>


            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="./public/uploads/banner1.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">

      </div>
    </div>
    <div class="carousel-item">
      <img src="./public/uploads/banner2.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">

      </div>
    </div>
    <div class="carousel-item">
      <img src="./public/uploads/banner3.jpg" class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">

      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Trang trước</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Trang sau</span>
  </a>
</div>



<section class="women-product-area section-gap" id="women">
	<!-- <div class="overlay overlay-bg"></div> -->
	<div class="container">
		<?php foreach ($data as $key => $value): ?>
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-40">
					<div class="title text-center">
						<h1 class="pt-60"><?php echo $key ?></h1>

					</div>
				</div>
			</div>
			<div class="row">
				<?php foreach ($value as $item): ?>
					<div class="col-lg-3 col-md-6 single-product">
						<div class="content">
							<div class="content-overlay"></div>
							<img class="content-image img-fluid d-block mx-auto"
							src="<?php echo uploads() ?>product/<?php echo $item['image'] ?>" alt="">

							<div class="content-details fadeIn-bottom">
								<div class="bottom d-flex align-items-center justify-content-center">
									<a href="addcart.php?id=<?php echo $item['id'] ?>"><span class="lnr lnr-cart"></span></a>
									<a href="chi-tiet-san-pham.php?id=<?php echo $item['id'] ?>"><span class="lnr lnr-frame-expand"></span></a>

								</div>
							</div>
						</div>
						<div class="price">
							<h5 ><?php echo $item['name'] ?></h5>
							<h3 ><?php if ($item['sale']>0): ?>
							<span class="ml-10"><?php echo formatpricesale($item['price'],$item['sale'])?></span>
						  &ensp;<strike><?php echo formatPrice($item['price']) ?></strike>
						 <?php else: ?>
						 	<span class="ml-10"><?php echo formatpricesale($item['price'],$item['sale'])?></span>
						<?php endif ?></h3>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		<?php endforeach ?>
	</div>
</section>
<?php  require_once __DIR__. "/layouts/footer.php";?>
