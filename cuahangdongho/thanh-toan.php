<?php
require_once __DIR__. "/autoload/autoload.php";

if( !isset($_SESSION['name_id']))
{
	echo "<script>alert('Bạn phải đăng nhập mới thanh toán hàng');location.href='index.php'</script>";

}
$user=$db->fetchID("users", intval($_SESSION['name_id']));

if($_SERVER["REQUEST_METHOD"]=="POST")
{
	$data=
	[

		'amount' => $_SESSION['total'],
		'users_id'=> $_SESSION['name_id'],
		'note'    => postInput("note")
	];

	$idtran= $db->insert("transaction", $data);
	if($idtran>0)
	{
		foreach ($_SESSION['cart'] as $key => $value)
		{
			$data2=
			[
				'transaction_id'=> $idtran,
				'product_id' 	=> $key,
				'qty' 			=> $value['qty'],
				'price' 		=> $value['price']
			];
			$id_insert = $db->insert("orders", $data2);
		}

		$_SESSION['success']="Lưu đơn hàng thành công";
		header("location: thong-bao.php");
	}
}
?>

<?php  require_once __DIR__. "/layouts/header.php";?>

<!-- Start women-product Area -->
<section class="women-product-area section-gap" id="women">
	<div class="container">
		<form action="" method="POST" class="billing-form">
			<div class="row">
				<div class="col-lg-8 col-md-6">
					<h3 class="billing-title mt-20 mb-10">Thông tin thanh toán</h3>
					<div class="row">
						<input type="text" readonly="" name="name" placeholder="Full name*" onfocus="this.placeholder=''"
						onblur="this.placeholder = 'Full name*'"  class="common-input mt-20" value="<?php echo $user['name'] ?>">

						<input type="email" readonly="" name="email" placeholder="Email address*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Email address*'"  class="common-input mt-20" value="<?php echo $user['email'] ?>">

						<input type="number" readonly="" name="phone" placeholder="Phone number*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Phone number*'"  class="common-input mt-20" value="<?php echo $user['phone'] ?>">

						<input type="text" readonly="" name="address" placeholder="Địa chỉ*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Địa chỉ*'"  class="common-input mt-20" value="<?php echo $user['address'] ?>">

						<input type="text" readonly="" name="sotien" placeholder="Tổng tiền*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Tổng tiền*'"  class="common-input mt-20" value="<?php echo formatPrice($_SESSION['total']) ?>">

						<input type="text"  name="note" placeholder="Ghi chú*" onfocus="this.placeholder=''" onblur="this.placeholder = 'Ghi chú*'"  class="common-input mt-20" value="">
						<button type="submit" class="view-btn color-2 mt-20 w-100"><span>Xác nhận thanh toán</span></button>
					</div>
				</div>
				<!-- <div class="col-lg-4 col-md-6">
					<div class="order-wrapper mt-50">
						<h3 class="billing-title mb-10">Your Order</h3>
						<div class="order-list">
							<div class="list-row d-flex justify-content-between">
								<div>Product</div>
								<div>Total</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<div>Pixelstore fresh Blackberry</div>
								<div>x 02</div>
								<div>$720.00</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<div>Pixelstore fresh Blackberry</div>
								<div>x 02</div>
								<div>$720.00</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<div>Pixelstore fresh Blackberry</div>
								<div>x 02</div>
								<div>$720.00</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<h6>Subtotal</h6>
								<div>$2160.00</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<h6>Shipping</h6>
								<div>Flat rate: $50.00</div>
							</div>
							<div class="list-row d-flex justify-content-between">
								<h6>Total</h6>
								<div class="total">$2210.00</div>
							</div>
							<div class="d-flex align-items-center mt-10">
								<input class="pixel-radio" type="radio" id="check" name="brand">
								<label for="check" class="bold-lable">Check payments</label>
							</div>
							<p class="payment-info">Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
							<div class="d-flex justify-content-between">
								<div class="d-flex align-items-center">
									<input class="pixel-radio" type="radio" id="paypal" name="brand">
									<label for="paypal" class="bold-lable">Paypal</label>
								</div>
								<img src="img/organic-food/pm.jpg" alt="" class="img-fluid">
							</div>
							<p class="payment-info">Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
							<div class="mt-20 d-flex align-items-start">
								<input type="checkbox" class="pixel-checkbox" id="login-4">
								<label for="login-4">I’ve read and accept the <a href="#" class="terms-link">terms & conditions*</a></label>
							</div>
							<button class="view-btn color-2 w-100 mt-20"><span>Proceed to Checkout</span></button>
						</div>
					</div>
				</div> -->
			</div>
		</form>
	</div>
</section>
<!-- End women-product Area -->
<?php  require_once __DIR__. "/layouts/footer.php";?>
