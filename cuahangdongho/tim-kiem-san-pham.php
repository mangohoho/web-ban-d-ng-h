<?php
require_once __DIR__. "/autoload/autoload.php";


    $search = addslashes($_GET['search']);

		if(isset($_GET['p']))
		{
			$p=$_GET['p'];
		}
		else
		{
			$p=1;
		}
		$sql = "SELECT * FROM product WHERE name like '%$search%'";
		$total=count($db->fetchsql($sql));

		$product= $db->fetchJones("product",$sql,$total,$p,4,true);
		$sotrang=$product['page'];
		unset($product['page']);

		$path=$_SERVER['SCRIPT_NAME'];


?>

<?php  require_once __DIR__. "/layouts/header.php";?>


<div class="countdown-content pb-40">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-8 col-md-7">

			<section class="lattest-product-area pb-40 category-list">
					<br><br>
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-40">
							<div class="title text-center">

							<p></p>
							</div>
						</div>
					</div>

					<div class="row ">
						<?php foreach ($product as $item): ?>
							<div class="col-xl-3 col-lg-6 col-md-12 col-sm-6 single-product">
								<div class="content">
									<div class="content-overlay"></div>
									<img class="content-image img-fluid d-block mx-auto"
									src="<?php echo uploads() ?>product/<?php echo $item['image'] ?>" alt="">
									<div class="content-details fadeIn-bottom">
										<div class="bottom d-flex align-items-center justify-content-center">

											<a href="addcart.php?id=<?php echo $item['id'] ?>"><span class="lnr lnr-cart"></span></a>
											<a href="chi-tiet-san-pham.php?id=<?php echo $item['id'] ?>">
												<span class="lnr lnr-frame-expand"></span>
											</a>
										</div>
									</div>
								</div>
								<div class="price">
									<h5 ><?php echo $item['name'] ?></h5>
									<h3 ><?php if ($item['sale']>0): ?>
							<span class="ml-10"><?php echo formatpricesale($item['price'],$item['sale'])?></span>
						  &ensp;<strike><?php echo formatPrice($item['price']) ?></strike>
						 <?php else: ?>
						 	<span class="ml-10"><?php echo formatpricesale($item['price'],$item['sale'])?></span>
						<?php endif ?></h3>
								</div>
							</div>
						<?php endforeach ?>
					</div>

				</section>

				<div class="filter-bar d-flex flex-wrap align-items-center">

					<div class="pagination">

						<?php for ($i=1; $i <= $sotrang ; $i++) :?>
							<a class="<?php echo isset($_GET['p']) && $_GET['p']==$i ? 'active' : '' ?>"
								href="<?php echo $path ?>?id=<?php echo $id ?>&&p=<?php echo $i ?>">
								<?php echo $i ?></a>
						<?php endfor; ?>
					</div>
				</div>

				</div>

			</div>
		</div>
	</div>

<?php  require_once __DIR__. "/layouts/footer.php";?>
